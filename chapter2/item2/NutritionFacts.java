package chapter2.item2;

public class NutritionFacts{
    // required
    private final int servingSize;
    private final int servings;

    // optional
    private final int calories;
    private final int fat;
    private final int sodium;
    private final int carbohydrate;
    
    
    public static class Builder{
        private final int servingSize;
        private final int servings;
        
        // 생성시 입력이 안될 수도 있는 optional variables는 디폴트값 설정
        private int calories = 0;
        private int fat = 0;
        private int sodium = 0;
        private int carbohydrate = 0;

        public Builder(int servingsSize, int servings){
            this.servingsSize = servingsSize;
            this.servings = servings;
        }

        public Builder carlories(int val){
            this.calories = val; return this;
        }

        public Builder fat(int val){
            this.fat = val; return this;
        }

        public Builder sodium(int val){
            this.sodium = val; return this;
        }

        public Builder carbohydrate(int val){
            this.carbohydrate = val; return this;
        }

        public NutritionFacts build(){
            return new NutritionFacts(this);
        }
    }

    private NutritionFacts(Builder builder){
        servingSize = builder.servingSize;
        servings = builder.servings;
        calories = builder.calories;
        fat = builder.fat;
        sodium = builder.sodium;
        carbohydrate = builder.carbohydrate;
    }

    public static void main(String[] args) {
        NutritionFacts 
    }
}