package chapter2.item4;

import com.sun.xml.internal.ws.policy.spi.AssertionCreationException;

public class NoConstructor{
    private NoConstructor() {
        // throw new AssertionError();
    }

    public static void main(String[] args) {
        NoConstructor instance = new NoConstructor();
    }
}