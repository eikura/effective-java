package chapter2.item5;

import java.util.*;

public class Person{
    private final Date birthDate;

    private static final Date BOOM_START;
    private static final Date BOOM_END;

    Person(Date birthDate){
        this.birthDate = birthDate;
    }

    static{
        Calendar gmtCal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        gmtCal.set(1946, Calendar.JANUARY, 1, 0, 0, 0);
        BOOM_START = gmtCal.getTime();
        gmtCal.set(1965, Calendar.JANUARY, 1, 0, 0, 0);
        BOOM_END = gmtCal.getTime();
    }

    public boolean isBabyBoomer() {
        return this.birthDate.compareTo(BOOM_START) >= 0 && this.birthDate.compareTo(BOOM_END) < 0;
    }

    public String toString(){
        return this.birthDate.toString();
    }

    public static void main(String[] args) {
        for(int i = 0 ; i < 100000 ; i++){
            Person a = new Person(new Date());
            System.out.println(a.toString());
        }
    }
}