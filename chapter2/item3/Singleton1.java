package chapter2.item3;

public class Singleton1{
    private Singleton1(){}

    public static Singleton1 getInstance(){
        return LazyHolder.INSTANCE;
    }

    private static class LazyHolder{
        private static final Singleton1 INSTANCE = new Singleton1();
    }


    public static void main(String[] args) {
        
        System.out.println(Singleton1.getInstance());
        System.out.println(new Singleton1().getInstance());
        System.out.println(new Singleton1().getInstance());
    }
}