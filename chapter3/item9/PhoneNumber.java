package chapter3.item9;

import java.util.*;

public class PhoneNumber{
    private final short areaCode;
    private final short prefix;
    private final short lineNumber;


    // equals가 true인데 hashCode가 같지 않아서 생기는 문제
    public PhoneNumber(int areaCode, int prefix, int lineNumber){
        rangeCheck(areaCode, 999, "area code");
        rangeCheck(prefix, 999, "prefix");
        rangeCheck(lineNumber, 9999, "line number");

        this.areaCode = (short) areaCode;
        this.prefix = (short) prefix;
        this.lineNumber = (short) lineNumber;
    }

    private static void rangeCheck(int arg, int max, String name){
        if(arg < 0 || arg > max){
            throw new IllegalArgumentException(name + ": " + arg);
        }
    }

    @Override public boolean equals(Object o){
        if ( o == this ) return true;
        if(!(o instanceof PhoneNumber)) return false;
        PhoneNumber pn = (PhoneNumber) o;
        return pn.lineNumber == lineNumber && pn.prefix == prefix && pn.areaCode == areaCode;
    }

    public static void main(String[] args) {
        Map<PhoneNumber, String> m = new HashMap<PhoneNumber, String>();

        m.put(new PhoneNumber(707, 867, 5309),"Jenny");

        // 새로 생성된 PhoneNumber 인스턴스는 인스턴스 변수의 값은 기존과 같지만 다른 해쉬코드를 가짐.
        System.out.println(m.get(new PhoneNumber(707, 867, 5309)).toString());
    }

}